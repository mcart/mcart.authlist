<?

IncludeModuleLangFile(__FILE__);

if (class_exists("mcart_authlist"))
    return;

Class mcart_authlist extends CModule {

    var $MODULE_ID = "mcart.authlist";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_GROUP_RIGHTS = "Y";

    function mcart_authlist() {
        $arModuleVersion = array();
        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path . "/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        } else {
            $this->MODULE_VERSION = AUTHLIST_VERSION;
            $this->MODULE_VERSION_DATE = AUTHLIST_VERSION_DATE;
        }
        $this->MODULE_NAME = GetMessage("AUTHLIST_MODULE_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("AUTHLIST_MODULE_NAME");

        $this->PARTNER_NAME = GetMessage("PARTNER_NAME");
        $this->PARTNER_URI = "http://mcart.ru/";
    }

    function DoInstall() {
        global $APPLICATION;

        if (!IsModuleInstalled("mcart.authlist")) {
            $this->InstallDB();
            $this->InstallEvents();
            $this->InstallFiles();
        }
        return true;
    }

    function DoUninstall() {
        $this->UnInstallDB();
        $this->UnInstallEvents();
        $this->UnInstallFiles();

        return true;
    }

    function InstallDB() {
        RegisterModule("mcart.authlist");
        return true;
    }

    function UnInstallDB() {
        UnRegisterModule("mcart.authlist");
        return true;
    }

    function InstallEvents() {
        RegisterModuleDependences("main", "OnAfterUserAuthorize", "mcart.authlist", 'CMcartAuthList', 'OnAfterUserAuthorize');
        RegisterModuleDependences("ldap", "OnBeforeUserLogin", "mcart.authlist", 'CMcartAuthList', 'OnBeforeUserLogin');
        CAgent::AddAgent(
            "CMcartAuthList::DeleteOldElement();",
            "mcart.authlist",
            "Y",
            60*60*24*30,
            "",
            "Y",
            "", 
            30);
        return true;
    }

    function UnInstallEvents() {
        UnRegisterModuleDependences("main", "OnAfterUserAuthorize", "mcart.authlist", 'CMcartAuthList', 'OnAfterUserAuthorize');
        UnRegisterModuleDependences("ldap", "OnBeforeUserLogin", "mcart.authlist", 'CMcartAuthList', 'OnBeforeUserLogin');
        CAgent::RemoveModuleAgents("mcart.rememberrichdial");
        return true;
    }

    function InstallFiles() {
        return true;
    }

    function UnInstallFiles() {
        return true;
    }

}

//end class
?>