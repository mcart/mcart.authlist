<?
IncludeModuleLangFile(__FILE__);
class CMcartAuthList {
    function OnAfterUserAuthorize($arFields) {
        $USER_IGNOR = COption::GetOptionInt("mcart.authlist", "USER_IGNOR");
        if($arFields['user_fields']['ID'] != $USER_IGNOR && CModule::IncludeModule("iblock")) {
            $IBLOCK_ID = COption::GetOptionString("mcart.authlist", "IBLOCK_ID");
            if(!$IBLOCK_ID){
                $res = CIBlock::GetList(
                    Array(), 
                    Array(
                        "CODE"=>'userauthlist'
                    ), true
                );
                if($ar_res = $res->Fetch())
                {
                    $IBLOCK_ID = $ar_res['ID'];
                    COption::SetOptionString("mcart.authlist", "IBLOCK_ID", $IBLOCK_ID);
                }
            }
            if($IBLOCK_ID){
                $el = new CIBlockElement;
                $PROP = array();
                $PROP['USER_AGENT'] = $_SERVER['HTTP_USER_AGENT'];
                $PROP['IP'] = $_SERVER['REMOTE_ADDR'];

                $arLoadProductArray = Array(
                    'CREATED_BY' => $arFields['user_fields']['ID'],
                    "IBLOCK_ID"      => $IBLOCK_ID,
                    "PROPERTY_VALUES"=> $PROP,
                    "NAME"           => "Авторизация пользователя",
                    "ACTIVE"         => "Y",
                );
                $el->Add($arLoadProductArray);
            }
        }
    }
    function DeleteOldElement() {
        $IBLOCK_ID = COption::GetOptionString("mcart.authlist", "IBLOCK_ID");
        $n_mon = COption::GetOptionString("mcart.authlist", "N_MON", 2);;
        $date_mk = mktime(0, 0, 0, date("m")-$n_mon, date("d"), date("Y"));
        global $DB;
        $date = date($DB->DateFormatToPHP(CLang::GetDateFormat("SHORT")), $date_mk);
        $filter = Array('IBLOCK_ID' => $IBLOCK_ID, '<DATE_CREATE' => $date);
        $arSelect = array('ID','DATE_CREATE');

        $rsElements = CIBlockElement::GetList(Array("SORT"=>"ASC"), $filter, false, Array(), $arSelect);
        while($arElement = $rsElements->Fetch()){
            CIBlockElement::Delete($arElement['ID']);
        }
        
        return "CMcartAuthList::DeleteOldElement();";  
    }
    function OnBeforeUserLogin($arFields){
        $rsUser = CUser::GetByLogin($arFields['LOGIN']);
        $arUser['user_fields'] = $rsUser->Fetch();
        
        CMcartAuthList::OnAfterUserAuthorize($arUser);
    } 
}
?>
